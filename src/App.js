import React from "react";
import { useEffect, useState } from "react";
import './App.css';

function App() {
  const [equipamentos, setEquipamentos] = useState(['']);

  function handleAdd(event) {
    event.preventDefault();
    const form = event.target;

    const codigoPatrimonio = form.codigoPatrimonio.value;
    const numeroSerie = form.numeroSerie.value;
    const descricao = form.descricao.value;
    const tipo = form.tipo.value;
    const dadosEquipamento = {
      id: new Date().getTime(),
      codigoPatrimonio,
      numeroSerie,
      descricao,
      tipo,
    };    

    if (!codigoPatrimonio || codigoPatrimonio === "") {
      return alert("Código do patrimonio invalido, favor preencher!");
    } else if (!numeroSerie || numeroSerie === "") {
      return alert("Numero de serie invalido, favor preencher!");
    } else if (!descricao || descricao === "") {
      return alert("Descrição invalida, favor preencher!");
    } else if (!tipo || tipo === "") {
      return alert("Tipo invalido, favor preencher!");
    } else {
      const novosDados = [...equipamentos, dadosEquipamento];
      setEquipamentos(novosDados);
      localStorage.setItem("equipamentos", JSON.stringify(novosDados));
    }
  }

  function handleDelete(id) {
    const novosDados = equipamentos.filter(
      (equipamento) => equipamento.id !== id);
    localStorage.setItem("equipamentos", JSON.stringify(novosDados));
    setEquipamentos(novosDados);
  }

  useEffect(() => {
    function loadData() {
      const dados = localStorage.getItem("equipamentos");
      setEquipamentos(JSON.parse(dados) || []); //vai converter a string para objeto JS
    }
    loadData();
  });

  return (
    <>
    <div class="formInput">
      <form onSubmit={handleAdd}>
          <input
            type="text"
            name="codigoPatrimonio"
            placeholder="Código do patrimônio"
          />
          <input type="text" name="numeroSerie" placeholder="Número de série" />
          <input type="text" name="descricao" placeholder="Descrição" />
          <input type="text" name="tipo" placeholder="Tipo" />

          <input type="submit" value="Salvar" />
      </form>

      <table>
        <thead>
          <tr>
            <th>Cod. Patrimonio</th>
            <th>Número de Série</th>
            <th>Descrição</th>
            <th>Tipo</th>
          </tr>
        </thead>
        <tbody>
          {equipamentos.map((equipamento) => (
            <tr key={equipamento.id}>
              <td>{equipamento.codigoPatrimonio}</td>
              <td>{equipamento.numeroSerie}</td>
              <td>{equipamento.descricao}</td>
              <td>{equipamento.tipo}</td>
              <td>
                <button
                  className="Excluir"
                  onClick={() => handleDelete(equipamento.id)}>
                  Excluir
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      </div>
    </>
  );
}

export default App;
